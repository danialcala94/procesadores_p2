// Author: ALCAL� VALERA, DANIEL

package myjflex;
//Area de codigo, importaciones y paquetes

import java.io.*;
%%
//Area de opciones y declaraciones
%class LexerOne
%standalone
%public


digito = [0-9]
impar = [13579]
par = [02468]


%%
//Area de reglas y acciones
{digito}*{par} {System.out.print("NUM_PAR");}
{digito}*{impar} {System.out.print("NUM_IMPAR");}
