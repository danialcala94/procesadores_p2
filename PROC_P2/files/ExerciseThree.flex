// Author: ALCAL� VALERA, DANIEL

package myjflex;
//Area de codigo, importaciones y paquetes

import java.io.*;
%%
//Area de opciones y declaraciones
%class LexerThree
%standalone
%public
%line


word = [a-z]+
number = [0-9]+


%%
//Area de reglas y acciones
{word} { System.out.print(yytext().toUpperCase()); }
{number} { System.out.print((Integer.parseInt(yytext()) * (yyline + 1)) + ""); }