// Author: ALCAL� VALERA, DANIEL

package myjflex;
//Area de codigo, importaciones y paquetes

import java.io.*;
%%
//Area de opciones y declaraciones
%class LexerFour
%standalone
%public

Blanco = [ ]+
Tabulador = [\t]+

%%
//Area de reglas y acciones
{Blanco} 	{ System.out.print(" "); }
{Tabulador} 	{ System.out.print("\t"); }
^[//][//].*  { System.out.print(""); }
