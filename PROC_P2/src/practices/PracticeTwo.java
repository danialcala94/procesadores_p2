/*  Author: ALCAL� VALERA, DANIEL
 *  Practice: Pr�ctica 2 JFlex
 */
package practices;

import java.io.File;

import myjflex.LexerFive;
import myjflex.LexerFour;
import myjflex.LexerOne;
import myjflex.LexerThree;
import myjflex.LexerTwo;

public class PracticeTwo {
	
	// Install and running JFlez
	private static final String OUTPUT_DIR = "src/myjflex/";
	private static final String FILES_DIR = "files/";
	private static final String EXERCISE_ONE = FILES_DIR + "ExerciseOne.flex";
	private static final String EXERCISE_TWO = FILES_DIR + "ExerciseTwo.flex";
	private static final String EXERCISE_THREE = FILES_DIR + "ExerciseThree.flex";
	private static final String EXERCISE_FOUR = FILES_DIR + "ExerciseFour.flex";
	private static final String EXERCISE_FIVE = FILES_DIR + "ExerciseFive.flex";
	private static final String EXAMPLE_ONE = FILES_DIR + "exOne.txt";
	private static final String EXAMPLE_TWO = FILES_DIR + "exTwo.txt";
	private static final String EXAMPLE_THREE = FILES_DIR + "exThree.txt";
	private static final String EXAMPLE_FOUR = FILES_DIR + "exFour.txt";
	private static final String EXAMPLE_FIVE = FILES_DIR + "exFive.txt";
	
	/**
	 * Generates the five analyzers and analyze the file with them.
	 */
	public static void execute() {
		String paths[] = {EXERCISE_ONE, EXERCISE_TWO, EXERCISE_THREE, EXERCISE_FOUR, EXERCISE_FIVE};
		generateAllAnalyzers(paths, OUTPUT_DIR);
		
		String arguments[][] = {{EXAMPLE_ONE}, {EXAMPLE_TWO}, {EXAMPLE_THREE}, {EXAMPLE_FOUR}, {EXAMPLE_FIVE}};
		useAllAnalyzers(arguments);
	}
	
	/**
	 * Deletes the previous analyzers if existing.
	 * @return
	 */
	private static boolean deletePreviousAnalyzers() {
		int cont = 0;
		
		if (new File(OUTPUT_DIR + "LexerOne.java").delete()) {
			cont++;
		}
		if (new File(OUTPUT_DIR + "LexerTwo.java").delete()) {
			cont++;
		}
		if (new File(OUTPUT_DIR + "LexerThree.java").delete()) {
			cont++;
		}
		if (new File(OUTPUT_DIR + "LexerFour.java").delete()) {
			cont++;
		}
		if (new File(OUTPUT_DIR + "LexerFive.java").delete()) {
			cont++;
		}
		
		System.out.println("Deleted " + cont + " analyzers.");
		
		if (cont == 5) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * It creates an analyzer using JFlex.
	 * @param path
	 * @param outputDir
	 */
	private static void generate(String path, String outputDir) {
		// https://www.youtube.com/watch?v=w-KfjJdRas8 10:28
		String args[] = {path, "-d", outputDir};
		jflex.Main.main(args);
	}
	
	/**
	 * Generates the five analyzers for the first practice.
	 * @param path
	 * @param outputDir
	 */
	private static void generateAllAnalyzers(String[] path, String outputDir) {
		deletePreviousAnalyzers();
		
		for (int i = 0; i < path.length; i++) {
			System.out.println("Generating analyzer number " + (i + 1));
			generate(path[i], outputDir);
		}
	}

	
	/**
	 * Analyze the file with the five analyzers generated.
	 * @param fileToAnalyze
	 */
	private static void useAllAnalyzers(String[][] fileToAnalyze) {
		System.out.println("\nExecuting LexerOne: ");
		LexerOne.main(fileToAnalyze[0]);
		System.out.println("\nExecuting LexerTwo: ");
		LexerTwo.main(fileToAnalyze[1]);
		System.out.println("\nExecuting LexerThree: ");
		LexerThree.main(fileToAnalyze[2]);
		System.out.println("\nExecuting LexerFour: ");
		LexerFour.main(fileToAnalyze[3]);
		System.out.println("\nExecuting LexerFive: ");
		LexerFive.main(fileToAnalyze[4]);
	}
}

